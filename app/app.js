﻿/**
 * Xavi Majoral: xavimajoral@gmail.com
 * Game Duell FrontEnd Test
 * 23-24/08/2014
 */

var gameApp = angular.module('gameApp', ['ngRoute']);

gameApp.config(function ($routeProvider) {
    $routeProvider
        .when('/onGoingGames',
            {
                controller: 'SimpleController',
                templateUrl: 'app/partials/onGoingGames.html'
            })
        .when('/finishedGames',
            {
                controller: 'SimpleController',
                templateUrl: 'app/partials/finishedGames.html'
            })
        .otherwise({ redirectTo: '/onGoingGames' });
});




