﻿/**
 * Xavi Majoral: xavimajoral@gmail.com
 * Game Duell FrontEnd Test
 * 23-24/08/2014
 */

gameApp.factory('simpleFactory', function() {
    var games = [
        {
            name: 'Prince of Persia',
            gameType: 'Platform',
            startTime: 10,
            scores: 1,
            img: 'app/img/games/prince.png',
            finished: false
        },
        {
            name: 'Portal',
            gameType: 'Puzzle',
            startTime: 9,
            scores: 2,
            img: 'app/img/games/portal.jpg',
            finished: false
        },
        {
            name: 'Another World',
            gameType: 'Platform',
            startTime: 8,
            scores: 3,
            img: 'app/img/games/another.jpg',
            finished: true
        },
        {
            name: 'Borderlands 2',
            gameType: 'Action',
            startTime: 7,
            scores: 4,
            img: 'app/img/games/borderlands.jpeg',
            finished: false
        },
        {
            name: 'Lemmings',
            gameType: 'Logic',
            startTime: 6,
            scores: 5,
            img: 'app/img/games/lemmings.jpg',
            finished: true
        },
        {
            name: 'The Secret of Monkey Island',
            gameType: 'Adventure',
            startTime: 5,
            scores: 6,
            img: 'app/img/games/smi.jpg',
            finished: true
        },
        {
            name: 'Day of the Tentacle',
            gameType: 'Adventure-Puzzle',
            startTime: 4,
            scores: 7,
            img: 'app/img/games/tentacle.jpg',
            finished: true
        },
        {
            name: 'Grand Theft Auto',
            gameType: 'Action-Adventure',
            startTime: 3,
            scores: 8,
            img: 'app/img/games/gta.jpg',
            finished: false
        },
        {
            name: 'Metal Gear Solid',
            gameType: 'Platform',
            startTime: 2,
            scores: 9,
            img: 'app/img/games/mgs.jpg',
            finished: false
        },
        {
            name: 'Machinarium',
            gameType: 'Point & Click',
            startTime: 1,
            scores: 10,
            img:'app/img/games/machinarium.jpg',
            finished: true
        }
    ];

    var factory = {};

    factory.getGames = function() {
        return games;
    };

    return factory;
});