﻿/**
 * Xavi Majoral: xavimajoral@gmail.com
 * Game Duell FrontEnd Test
 * 23-24/08/2014
 */

gameApp.controller('SimpleController', function ($scope, simpleFactory) {
    $scope.games = [];

    init();

    function init() {
        $scope.games = simpleFactory.getGames();
    }

    $scope.addGame = function() {
        $scope.games.push(
            {
                name: $scope.newGame.name,
                company: $scope.newGame.company
            });
    }
});

gameApp.controller('ChildController', function ($scope) {
    $scope.orderby = 'name';
    $scope.reverse = false;

    $scope.setOrder = function (orderby) {
        if (orderby === $scope.orderby)
        {
            $scope.reverse = !$scope.reverse;
        }
        $scope.orderby = orderby;
    };

});